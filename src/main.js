import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './App.vue';
import BaseModal from './components/BaseModal.vue';
import CourseGoals from './pages/CourseGoals.vue';
import OtherStuff from './pages/OtherStuff.vue';

const app = createApp(App);
const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', component: OtherStuff },
    { path: '/goals', component: CourseGoals }
  ]
});

app.component('base-modal', BaseModal);

app.use(router);

router.isReady().then(function() {
  app.mount('#app');
});
/* Since usually, when app gets mounted, and page loads, router is not really aware what page should be loaded, and it introduces a bit of delay to it, if you are using
transitions or animations on routes, like we do in App.vue, there will be starting animation whenever the router is ready. Sometimes it's not even noticable, but if you
use some slower animations, or in some specific cases, it bothers the users, and it's not very pleasant.
In order to avoid that, we have to mount the app, whenever the router is ready, so it doesn't show blank page initially and then load the page.
The above case is exactly how to solve that. We use isReady function on the router, which returns a promise, and .then we can execute a function that mounts the app,
solving exactly the issue described above. */
